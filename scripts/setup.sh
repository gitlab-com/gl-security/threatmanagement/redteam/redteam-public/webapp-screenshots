#!/bin/bash

# create folder for downloaded binaries
mkdir bin

# install general pre-reqs
apt -qq update > /dev/null
apt -qq install -y wget unzip > /dev/null

# install pre-reqs for gowitness
apt -qq install -y chromium > /dev/null

# download tools
wget -q https://github.com/projectdiscovery/httpx/releases/download/v1.2.5/httpx_1.2.5_linux_amd64.zip
wget -q https://github.com/sensepost/gowitness/releases/download/2.4.2/gowitness-2.4.2-linux-amd64

# unzip / move all relases to bin folder
unzip httpx_1.2.5_linux_amd64.zip -d bin/
mv gowitness-2.4.2-linux-amd64 bin/gowitness
chmod u+x bin/gowitness
